package Model.Command;

import Model.Board;
import Util.Serialization;

import java.util.ArrayList;
import java.util.List;

public class ActionList {
    private Command back, forward;
    private int actualMemento = 0;
    private List<Memento> mementoList = new ArrayList<Memento>();

    public ActionList(Command back,Command forward){
        this.back = back;
        this.forward = forward;
    }
    public void setActionAtk(Command back){
        this.back = back;
    }
    public void setActionChangeMode(Command forward) {
        this.forward = forward;
    }
    public void add(){
        Memento state = new Memento(Integer.toString(actualMemento));
        mementoList.add(state);
        actualMemento = mementoList.indexOf(state)+1;
    }
    public Memento get(){
        Memento memento = mementoList.get(actualMemento-1);
        return memento;
    }
    public Memento getAndPop(){
        actualMemento --;
        Memento memento = mementoList.get(actualMemento);
        mementoList.remove(actualMemento);
        return memento;
    }
    public Board nextState(Board board){
        add();
        return forward.getChange(board, get().getState());
    }
    public Board lastState(Board board){
        if(mementoList.size() >= 3){
            Serialization.delete(getAndPop().getState());
            Serialization.delete(getAndPop().getState());
            return back.getChange(board, getAndPop().getState());
        }
        return null;
    }

}
