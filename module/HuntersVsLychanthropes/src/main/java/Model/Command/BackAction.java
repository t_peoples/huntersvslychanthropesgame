package Model.Command;

import Model.Board;
import Util.Serialization;

public class BackAction implements Command {
    @Override
    public Board getChange(Board board, String data) {
        Board newBoard = (Board) Serialization.deserialize(data);
        return newBoard;
    }
}
