package Model.Command;

import Model.Board;

public interface Command {
    public abstract Board getChange(Board board, String data );
}
