package Model.Command;

import java.util.UUID;
import Util.GlobalVariables;

public class Memento {
    private static final String token = GlobalVariables.getTrackFolder()+UUID.randomUUID().toString()+"#";
    private String state;

    public Memento(String state){
        this.state = token+state;
    }

    public String getState(){
        return state;
    }
}

