package Model.Factory;

import Model.Message.MessageGroup;
import Model.Piece.Human.Chieftain;
import Model.Piece.Human.Human;
import Model.Piece.Human.Hunter;
import Model.Piece.Human.Ranger;
import Model.Piece.Lycanthrope.AlphaWolf;
import Model.Piece.Lycanthrope.Lycanthrope;
import Model.Piece.Lycanthrope.ShadowWolf;
import Model.Piece.Lycanthrope.Werewolf;

public abstract class AbstractFactory {
    public abstract Human getHuman(Hunter hunter, int initialX, int initialY, MessageGroup response);

    public abstract Human getHuman(Chieftain chieftain, int initialX, int initialY, MessageGroup response);

    public abstract Human getHuman(Ranger ranger, int initialX, int initialY, MessageGroup response);

    public abstract Lycanthrope getLycanthrope(AlphaWolf alphaWolf, int initialX, int initialY, MessageGroup response);

    public abstract Lycanthrope getLycanthrope(ShadowWolf shadowWolf, int initialX, int initialY, MessageGroup response);

    public abstract Lycanthrope getLycanthrope(Werewolf werewolf, int initialX, int initialY, MessageGroup response);
}