package Model.Factory;

import Model.Message.MessageGroup;
import Model.Piece.Human.Chieftain;
import Model.Piece.Human.Human;
import Model.Piece.Human.Hunter;
import Model.Piece.Human.Ranger;
import Model.Piece.Lycanthrope.AlphaWolf;
import Model.Piece.Lycanthrope.Lycanthrope;
import Model.Piece.Lycanthrope.ShadowWolf;
import Model.Piece.Lycanthrope.Werewolf;
import Util.GlobalVariables;

public class HumanFactory extends AbstractFactory {


    @Override
    public Human getHuman(Hunter hunter, int initialX, int initialY, MessageGroup response) {
        return new Hunter(GlobalVariables.hunterId() + initialX + initialY, initialX, initialY, response);
    }

    @Override
    public Human getHuman(Chieftain chieftain, int initialX, int initialY, MessageGroup response) {
        return new Chieftain(GlobalVariables.chieftainId() + initialX + initialY, initialX, initialY, response);
    }

    @Override
    public Human getHuman(Ranger ranger, int initialX, int initialY, MessageGroup response) {
        return new Ranger(GlobalVariables.rangerId() + initialX + initialY, initialX, initialY, response);
    }

    @Override
    public Lycanthrope getLycanthrope(AlphaWolf alphaWolf, int initialX, int initialY,
                                      MessageGroup response) {
        return null;
    }


    @Override
    public Lycanthrope getLycanthrope(Werewolf werewolf, int initialX, int initialY, MessageGroup response) {
        return null;
    }

    @Override
    public Lycanthrope getLycanthrope(ShadowWolf shadowWolf, int initialX, int initialY,
                                      MessageGroup response) {
        return null;
    }
}