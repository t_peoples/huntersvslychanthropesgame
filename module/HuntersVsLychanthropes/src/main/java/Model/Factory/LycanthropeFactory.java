package Model.Factory;

import Model.Message.MessageGroup;
import Model.Piece.Human.Chieftain;
import Model.Piece.Human.Human;
import Model.Piece.Human.Hunter;
import Model.Piece.Human.Ranger;
import Model.Piece.Lycanthrope.AlphaWolf;
import Model.Piece.Lycanthrope.Lycanthrope;
import Model.Piece.Lycanthrope.ShadowWolf;
import Model.Piece.Lycanthrope.Werewolf;
import Util.GlobalVariables;

public class LycanthropeFactory extends AbstractFactory {

    @Override
    public Human getHuman(Hunter hunter, int initialX, int initialY, MessageGroup response) {
        return null;
    }

    @Override
    public Human getHuman(Chieftain chieftain, int initialX, int initialY, MessageGroup response) {
        return null;
    }

    @Override
    public Human getHuman(Ranger ranger, int initialX, int initialY, MessageGroup response) {
        return null;
    }

    @Override
    public Lycanthrope getLycanthrope(AlphaWolf alphaWolf, int initialX, int initialY,
                                      MessageGroup response) {
        return new AlphaWolf(GlobalVariables.alphaWolfId() + initialX + initialY, initialX, initialY, response);
    }


    @Override
    public Lycanthrope getLycanthrope(Werewolf werewolf, int initialX, int initialY, MessageGroup response) {
        return new Werewolf(GlobalVariables.werewolfId() + initialX + initialY, initialX, initialY, response);
    }

    @Override
    public Lycanthrope getLycanthrope(ShadowWolf shadowWolf, int initialX, int initialY,
                                      MessageGroup response) {
        return new ShadowWolf(GlobalVariables.shadowWolfId() + initialX + initialY, initialX, initialY, response);
    }
}
