package Model.Block;

import Util.GlobalVariables;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

public class Block implements BlockInterface
{
    private String image;
    private boolean movable, attackable;

    public Block(String image,boolean movable, boolean attackable)
    {
        setImage(image);
        this.movable = movable;
        this.attackable = attackable;
    }
    public ImageView getImage() {
        ImageView imgSrc = new ImageView();
        imgSrc.setImage(new Image(Thread.currentThread().getContextClassLoader().getResourceAsStream(image)));
        imgSrc.setFitHeight(GlobalVariables.sizeOfBlock());
        imgSrc.setFitWidth(GlobalVariables.sizeOfBlock());
        return imgSrc;
    }
    public boolean isMovable() {
        return movable;
    }
    public boolean isAttackable()
    {
        return attackable;
    }
    private void setImage(String image)
    {
        this.image = image;
    }
}
