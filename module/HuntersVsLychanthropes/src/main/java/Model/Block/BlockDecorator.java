package Model.Block;

import javafx.scene.image.ImageView;

public class BlockDecorator implements BlockInterface {

    protected BlockInterface decoratedBlock;

    public BlockDecorator(BlockInterface block) {
        decoratedBlock = block;
    }
    public ImageView getImage() {
        return decoratedBlock.getImage();
    }
    public boolean isMovable() {
        return decoratedBlock.isMovable();
    }
    public boolean isAttackable() {
        return decoratedBlock.isAttackable();
    }
}
