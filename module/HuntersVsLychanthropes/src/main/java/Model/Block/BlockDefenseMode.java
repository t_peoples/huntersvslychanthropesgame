package Model.Block;

import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

public class BlockDefenseMode extends BlockDecorator{
    public BlockDefenseMode(BlockInterface block) {
        super(block);
    }

    @Override
    public ImageView getImage() {
        DropShadow dropShadow = new DropShadow(20, Color.FIREBRICK);
        ImageView imageView = decoratedBlock.getImage();
        imageView.setEffect(dropShadow);
        return imageView;
    }
}
