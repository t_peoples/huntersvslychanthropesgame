package Model.Block;

import javafx.scene.image.ImageView;
public interface BlockInterface {

     ImageView getImage();
     boolean isMovable();
     boolean isAttackable();
}
