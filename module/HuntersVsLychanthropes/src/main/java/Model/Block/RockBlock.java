package Model.Block;

import Util.GlobalVariables;

public class RockBlock extends Block{

    public RockBlock(){
        super(GlobalVariables.rockFrame(),false,false);
    }
}
