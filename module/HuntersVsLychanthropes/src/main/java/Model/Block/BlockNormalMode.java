package Model.Block;

public class BlockNormalMode extends BlockDecorator {

    public BlockNormalMode(BlockInterface block) {
        super(block);
        getImage().setEffect(null);
    }
}
