package Model;

import Model.Block.BlockInterface;
import Model.Block.BlockDecorator;
import Model.Block.BlockNormalMode;
import Model.Block.GrassBlock;
import Model.Piece.Decorator.PieceNormalMode;
import Model.Piece.Decorator.PieceAttackMode;
import Model.Piece.Decorator.PieceDefenseMode;
import Model.Piece.PieceInterface;
import Util.GlobalVariables;
import Util.CircularArrayList;
import Model.Message.MessageGroup;
import Util.TextRepository;

public class Board {

    public static final int WIDTH = GlobalVariables.numberOfBlocksWidth();
    public static final int HEIGHT = GlobalVariables.numberOfBlocksHeight();
    public MessageGroup response;
    private BlockInterface[][] blocks;
    private CircularArrayList<Player> players;
    private CircularArrayList<Integer> timeOfDayList;

    public Board(Board board)
    {
        this.blocks = board.blocks;
        this.players = board.players;
        this.response = board.response;
        this.timeOfDayList = board.timeOfDayList;
    }

    public Board(CircularArrayList<Player> players,MessageGroup response,int timeOfDayDelay)
    {
        this.players = players;
        this.response = response;
        createTimeOfDayList(timeOfDayDelay);
        drawRetangularBoard();
        // Build initial message to start player
        this.response.addMessage(TextRepository.playerTurn(players.getCurrent()), GlobalVariables.informationCode());
    }

    private void drawRetangularBoard()
    {
        blocks = new BlockDecorator[WIDTH][HEIGHT];

        for(int i = 0; i < HEIGHT; i++)
        {
            for (int j = 0; j < WIDTH; j++)
            {
                blocks[i][j] = new BlockNormalMode(new GrassBlock());
            }
        }
    }

    public BlockInterface getBlock(int x, int y)
    {
        return blocks[x][y];
    }

    // Move the piece
    public boolean changePosition(PieceInterface piece, int x, int y) {
        int oldX, oldY;
        oldX = piece.getX();
        oldY = piece.getY();
        if (getCurrentPlayerTurn().getSelectedPiece().move(x, y)) {
            blocks[x][y] = blocks[oldX][oldY];
            blocks[oldX][oldY] = new BlockNormalMode(new  GrassBlock());
            return true;
        } else
            blocks[piece.getX()][piece.getY()] = piece;
        return false;
    }
    public void removeDeadPieces()
    {
        for(Player temp : players){
            PieceInterface piece = temp.removeDeadPiece();
            if (piece != null)
                blocks[piece.getX()][piece.getY()] = new BlockNormalMode(new GrassBlock());
        }

    }
    // Get the non-active players' pieces
    public PieceInterface getNonPlayerPiece(int x, int y) {
        for (Player temp : players)
            if(temp != players.getCurrent())
                if (temp.getPieceByCoord(x, y) != null)
                    return temp.getPieceByCoord(x, y);
        return null;
    }
    private void setPlayerPieces() {
        for (Player tempPlayer : players) {
            for (PieceInterface temp : tempPlayer.getPieces()) {
                blocks[temp.getX()][temp.getY()] = temp;
            }

        }
    }

    public boolean selectPiece(int x, int y) {
        getCurrentPlayerTurn().selectPiece(getCurrentPlayerTurn().getPieceByCoord(x, y));
        // If the player has selected a valid piece
        if (getCurrentPlayerTurn().getSelectedPiece() != null) {
            // Cycle to the next state
            nextState();
            return true;
        }
        return false;
    }
    public boolean movePiece(int x, int y) {
        if(getBlock(x, y).isMovable()){
            // Move the piece and cycle to the next state
            if (changePosition(getCurrentPlayerTurn().getSelectedPiece(), x, y)) {
                nextState();

                // Show the attack range of the selected piece
                return true;

            }
        }
        return false;
    }
    public boolean attackPiece(int x, int y) {
        if (getNonPlayerPiece(x, y) != null) {
            // Attack the other plays piece and cycle to the next state
            if (getCurrentPlayerTurn().getSelectedPiece().attack(getNonPlayerPiece(x, y), getCurrentPlayerTurn().getSelectedPiece().getDamage())) {
                nextState();

                return true;
            }
        }
        return false;
    }
    //TODO Refactor conside moving player methods to new classs
    public CircularArrayList<Player> getPlayers() {
        return players;
    }
    public Player getCurrentPlayerTurn() {
        return players.getCurrent();
    }
    public void setNextPlayerTurn() {
        players.setNext();
    }
    public int getState() {
        return players.getCurrent().getState();
    }
    // Determines what to do based on what the next state is
    public void nextState() {
        if(players.getCurrent().isAttacking())
        {
            players.getCurrent().setNextState();
            //changeTimeOfDay();
            removeDeadPieces();
            setNextPlayerTurn();
        }else{
            players.getCurrent().setNextState();
        }
        stateMessage();

    }
    /* If the player has selected a piece to move, but wants to choose another piece, this reverts the player state from
    move, to select. */
    public void prevState() {
        if(players.getCurrent().setPrevState())
            stateMessage();
    }

    public MessageGroup getResponse(){
        return response;
    }
    private void stateMessage(){
        // If the player has selected a piece, allow them to move the piece
        if (players.getCurrent().isSelecting()) {
            response.addMessage(TextRepository.playerTurn(players.getCurrent()), GlobalVariables.informationCode());
            // If the player has moved the piece, allow them to choose an attack target
        } else if (players.getCurrent().isMoving()) {
            response.addMessage(TextRepository.selectMove(), GlobalVariables.informationCode());
            // If the player has attacked a piece or skipped, allow the next player to take their turn.
        } else if (players.getCurrent().isAttacking()) {
            response.addMessage(TextRepository.selectTarget(), GlobalVariables.informationCode());
        }
    }


    public void SetBlockDefensive(){
        getCurrentPlayerTurn().changeSelectedPiece( new PieceDefenseMode( getCurrentPlayerTurn().getSelectedPiece()));
        blocks[getCurrentPlayerTurn().getSelectedPiece().getX()][getCurrentPlayerTurn().getSelectedPiece().getY()] = getCurrentPlayerTurn().getSelectedPiece();
    }
    public void SetBlockAttack(){
        getCurrentPlayerTurn().changeSelectedPiece( new PieceAttackMode( getCurrentPlayerTurn().getSelectedPiece()));
        blocks[getCurrentPlayerTurn().getSelectedPiece().getX()][getCurrentPlayerTurn().getSelectedPiece().getY()] = getCurrentPlayerTurn().getSelectedPiece();
    }
    public void SetBlockNormal() {
        getCurrentPlayerTurn().changeSelectedPiece( new PieceNormalMode( getCurrentPlayerTurn().getSelectedPiece()));
        blocks[getCurrentPlayerTurn().getSelectedPiece().getX()][getCurrentPlayerTurn().getSelectedPiece().getY()] = getCurrentPlayerTurn().getSelectedPiece();
    }
    // The creation of this list in fixed for a while, we still thinking the better way to do this, it might be by parameters
    private void createTimeOfDayList(int size) {
        timeOfDayList = new CircularArrayList<Integer>();
        for (int i = 0; i < size; i++)
            timeOfDayList.add(GlobalVariables.returnDay());
        for (int i = 0; i < size; i++)
            timeOfDayList.add(GlobalVariables.returnNight());
        timeChangeMessage();
    }
    // If it's day, make it night. If it's night, make it day
    public void changeTimeOfDay() {
        timeOfDayList.setNext();
        setPlayerBoost();
        timeChangeMessage();
    }
    // Time of day has 4 cycles for the day and night. So in total it's 8 cycles
    public int getTimeOfDay() {
        return timeOfDayList.getCurrent();
    }
    // Sets the player pieces to boosted depending on the time of day.
    private void setPlayerBoost() {
        for (Player player : players)
            player.setPieceBoost(timeOfDayList.getCurrent());
    }
    // Inform the user that the time has changed
    private void timeChangeMessage() {
        if (isDay())
            response.addMessage(TextRepository.dayEffect(), GlobalVariables.eventCode());
        else
            response.addMessage(TextRepository.nightEffect(), GlobalVariables.eventCode());
    }
    // Checks to see if the current time is Day
    private boolean isDay() {
        if (getTimeOfDay() == GlobalVariables.returnDay())
            return true;
        return false;
    }


}
