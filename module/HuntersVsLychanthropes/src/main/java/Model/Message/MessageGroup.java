package Model.Message;

import java.util.ArrayList;
import java.util.List;

public class MessageGroup {

    private List<Message> messageList;

    public MessageGroup(){
        messageList = new ArrayList<Message>();
    }
    public List<Message> getMessageList(){
        return messageList;
    }
    public void addMessage(String textMessage, char typeMessage){
        Message messageTemp = new Message(textMessage, typeMessage);
        messageList.add(messageTemp);
    }
    public void clearMessageList(){
        messageList.clear();
    }
    public boolean hasValidMessage(){
        return !messageList.isEmpty();
    }
}
