package Model.Message;


public class Message {

    private String messageText;
    private char MessageType;

    public Message(String messageText, char typeMessage){
        this.messageText = messageText;
        this.MessageType = typeMessage;
    }

    public String getMessageText()
    {
        return messageText;
    }

    public char getMessageType() {
        return MessageType;
    }
    public boolean hasValidMessage(){
        return messageText != null;
    }
}
