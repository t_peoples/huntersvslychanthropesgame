package Model.Piece.Decorator;

import Model.Block.BlockNormalMode;
import Model.Piece.PieceInterface;

public class PieceNormalMode extends PieceDecorator {

    public PieceNormalMode(PieceInterface piece){
        super(piece, new BlockNormalMode(piece));
    }
}
