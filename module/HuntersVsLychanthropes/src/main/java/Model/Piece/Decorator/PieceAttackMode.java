package Model.Piece.Decorator;

import Model.Block.BlockAttackMode;
import Model.Piece.PieceInterface;

public class PieceAttackMode extends PieceDecorator{
    public PieceAttackMode(PieceInterface piece) {
        super(piece, new BlockAttackMode(piece));
    }

    @Override
    public int getDamage(){
        return decoratedPiece.getDamage()*2;
    }
    @Override
    public boolean checkMove(int x, int y) {
        if(x != getX() && y !=getY())
            return false;
        return decoratedPiece.checkMove(x, y);
    }
    @Override
    public boolean move(int x, int y){
        if(x != getX() && y !=getY())
            return false;
        return decoratedPiece.move(x, y);
    }

}
