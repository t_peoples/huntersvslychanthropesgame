package Model.Piece.Decorator;

import Model.Block.BlockDefenseMode;
import Model.Piece.PieceInterface;

public class PieceDefenseMode extends PieceDecorator{

    public PieceDefenseMode(PieceInterface piece) {
        super(piece, new BlockDefenseMode(piece));
    }
    public void takeDamage(int attack){
        decoratedPiece.takeDamage(attack/2);
    }
    public boolean checkMove(int x,int y){
        if(x == getX() || y ==getY())
            return false;
        return decoratedPiece.checkMove(x,y);
    }
    public boolean move(int x, int y){
        if(x == getX() || y ==getY())
            return false;
        return decoratedPiece.move(x, y);
    }
}
