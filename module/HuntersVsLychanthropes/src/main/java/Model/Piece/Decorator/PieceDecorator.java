package Model.Piece.Decorator;

import Model.Block.BlockDecorator;
import Model.Piece.PieceInterface;

public class PieceDecorator extends BlockDecorator implements PieceInterface {

    protected PieceInterface decoratedPiece;

    public PieceDecorator(PieceInterface piece, BlockDecorator block)
    {
        super(block);
        decoratedPiece = piece;
    }

    @Override
    public String getID() {
        return decoratedPiece.getID();
    }

    @Override
    public int getAttackRange() {
        return decoratedPiece.getAttackRange();
    }

    @Override
    public void setAttackRange(int attackRange) {
        decoratedPiece.setAttackRange(attackRange);
    }

    @Override
    public int getDamage() {
        return decoratedPiece.getDamage();
    }

    @Override
    public void setDamage(int damage) {
        decoratedPiece.setDamage(damage);
    }

    @Override
    public int getMovementRange() {
        return decoratedPiece.getMovementRange();
    }

    @Override
    public void setMovementRange(int movementRange) {
        decoratedPiece.setMovementRange(movementRange);
    }

    @Override
    public int getLife() {
        return decoratedPiece.getLife();
    }

    @Override
    public int getX() {
        return decoratedPiece.getX();
    }

    @Override
    public void setX(float x) {
        decoratedPiece.setX(x);
    }

    @Override
    public int getY() {
        return decoratedPiece.getY();
    }

    @Override
    public void setY(float y) {
        decoratedPiece.setY(y);
    }

    @Override
    public boolean isBoosted() {
        return decoratedPiece.isBoosted();
    }

    @Override
    public void setBoosted(boolean state) {
        decoratedPiece.setBoosted(state);
    }

    @Override
    public void setBoostEffect(int daynight) {
        decoratedPiece.setBoostEffect(daynight);
    }
    @Override
    public void setBoostEffect(int damage, int attackRange, int movementRange, boolean state) {
        decoratedPiece.setBoostEffect(damage, attackRange, movementRange, state);
    }

    @Override
    public boolean move(int x, int y) {
        return decoratedPiece.move(x, y);
    }

    @Override
    public boolean attack(PieceInterface target, int damage) {
        return decoratedPiece.attack(target, damage);
    }

    @Override
    public void takeDamage(int attack) {
        decoratedPiece.takeDamage(attack);
    }

    @Override
    public boolean checkMove(int x, int y) {
        return decoratedPiece.checkMove(x, y);
    }

    @Override
    public boolean isInRange(PieceInterface target) {
        return decoratedPiece.isInRange(target);
    }

    @Override
    public boolean isInRange(int x, int y) {
        return decoratedPiece.isInRange(x, y);
    }
}
