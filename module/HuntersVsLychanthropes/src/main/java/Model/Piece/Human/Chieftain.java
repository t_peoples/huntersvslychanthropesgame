package Model.Piece.Human;

import Model.Message.MessageGroup;
import Util.GlobalVariables;

public class Chieftain extends Human{

    private final static int hitPoints = 180;
    private final static int attackRange = 2;
    private final static int damage = 80;
    private final static int movementRange = 3;
    private final static String image = GlobalVariables.chieftainFrame();

    public Chieftain(String id, int initialX, int initialY, MessageGroup response) {
        super(id, hitPoints, attackRange, damage, movementRange, initialX, initialY, false, response, image);
    }

    @Override
    public void setBoostEffect(int daynight) {

        // Boost piece attributes during the day
        if (daynight == GlobalVariables.returnDay() && !isBoosted()) {
            // +0 damage, +1 attack range, +1 movement range
            setBoostEffect(0, 1, 1, true);
        }

        // Revert boost when night time occurs
        else if (daynight == GlobalVariables.returnNight() && isBoosted()) {
            setBoostEffect(0, -1, -1, false);
        }
    }


}
