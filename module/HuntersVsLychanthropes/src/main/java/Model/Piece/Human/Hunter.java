package Model.Piece.Human;

import Model.Message.MessageGroup;
import Util.GlobalVariables;

public class Hunter extends Human {

    private final static int hitPoints = 100;
    private final static int attackRange = 1;
    private final static int damage = 20;
    private final static int movementRange = 3;
    private final static String image = GlobalVariables.hunterFrame();


    public Hunter(String id, int initialX, int initialY, MessageGroup response) {
        super(id, hitPoints, attackRange, damage, movementRange, initialX, initialY, false, response,image);
    }

    @Override
    public void setBoostEffect(int daynight) {

        // Boost piece attributes during the day
        if (daynight == GlobalVariables.returnDay() && !isBoosted()) {
            // +0 damage, +0 attack range, +2 movement range
            setBoostEffect(0, 0, 2, true);

        }
        // Revert boost when night time occurs
        else if (daynight == GlobalVariables.returnNight() && isBoosted()) {
            setBoostEffect(0, 0, -2, false);
        }
    }


}
