package Model.Piece;

import Model.Block.BlockInterface;

public interface PieceInterface extends BlockInterface {

    public String getID();
    public int getAttackRange();
    public void setAttackRange(int attackRange);
    public int getDamage();
    public void setDamage(int damage);
    public int getMovementRange();
    public void setMovementRange(int movementRange);
    public int getLife();
    public int getX();
    public void setX(float x) ;
    public int getY() ;
    public void setY(float y) ;
    // Checks if the piece is currently boosted
    // Used for day/night boosts
    public boolean isBoosted() ;
    public void setBoosted(boolean state) ;
    public abstract void setBoostEffect(int dayNight);
    public boolean move(int x, int y) ;
    public void setBoostEffect(int damage, int attackRange, int movementRange, boolean state);
    public boolean attack(PieceInterface target, int damage);
    public void takeDamage(int attack);
    public boolean checkMove(int x, int y);
    public boolean isInRange(PieceInterface target);
    public boolean isInRange(int x, int y);
}
