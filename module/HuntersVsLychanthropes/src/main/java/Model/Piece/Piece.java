package Model.Piece;

import Model.Block.Block;
import Model.Block.BlockDecorator;
import Model.Message.MessageGroup;
import Util.TextRepository;
import Util.GlobalVariables;


public abstract class Piece extends BlockDecorator implements PieceInterface{

    public MessageGroup response;
    // Piece attributes
    private String ID;
    private int hitPoints, attackRange, damage, movementRange, actualX, actualY;
    private boolean boosted;


    public Piece(Block decoratedBlock) {
        super(decoratedBlock);
    }

    public Piece(String ID, int hitPoints, int attackRange, int damage, int movementRange,
                 int initialX, int initialY, boolean boosted, MessageGroup response,String image) {
        super(new Block(image, false, true));
        this.ID = ID;
        this.hitPoints = hitPoints;
        this.attackRange = attackRange;
        this.damage = damage;
        this.movementRange = movementRange;
        actualX = initialX;
        actualY = initialY;
        this.boosted = boosted;
        this.response = response;
    }
    public String getID() {
        return this.ID;
    }
    public int getAttackRange() {
        return this.attackRange;
    }
    public void setAttackRange(int attackRange) {
        this.attackRange = attackRange;
    }
    public int getDamage() {
        return this.damage;
    }
    public void setDamage(int damage) {
        this.damage = damage;
    }
    public int getMovementRange() {
        return this.movementRange;
    }
    public void setMovementRange(int movementRange) {
        this.movementRange = movementRange;
    }
    public int getLife() {
        return this.hitPoints;
    }
    public int getX() {
        return this.actualX;
    }
    public void setX(float x) {
        actualX = (int) x;
    }
    public int getY() {
        return this.actualY;
    }
    public void setY(float y) {
        actualY = (int) y;
    }

    public boolean move(int x, int y) {
        if (checkMove(x, y)) {
            actualX = x;
            actualY = y;
            return true;
        }
        // If movement is out of range, inform the player to move again.
        response.addMessage(TextRepository.outOfMovementRange(), GlobalVariables.errorCode());
        return false;
    }
    //TODO perhaps refactor and create another decorator
    public void setBoostEffect(int damage, int attackRange,int movementRange, boolean state)
    {
        setDamage(getDamage() + damage);
        setAttackRange(getAttackRange() + attackRange);
        setMovementRange(getMovementRange() + movementRange);
        setBoosted(state);
    }

    public boolean attack(PieceInterface target, int damage){
        if (checkAttack(target) && isInRange(target)){
            target.takeDamage(damage);
            return true;
        }
        // If attack is out of range, inform the player to try again.
        response.addMessage(TextRepository.outOfAttackRange(),GlobalVariables.successCode());
        return false;
    }

    public void takeDamage(int attack){
        response.addMessage(TextRepository.attackSuccess(attack, this), GlobalVariables.successCode());
        hitPoints -= attack;
    }
    public boolean checkMove(int x,int y){
        // Performs a check to see if the movement is within the piece movement range
        if ((actualX != x || actualY != y)
                && (Math.abs(actualX - x) <= movementRange
                && Math.abs(actualY - y) <= movementRange)) {
            return true;
        }
        return false;
    }

    protected boolean checkAttack(PieceInterface target){
        if ((target.getID() != this.ID))
            return true;
        return false;
    }
    public boolean isInRange(PieceInterface target) {
        // Performs a check to see if the target piece is within attack range
        if (Math.abs(actualX - target.getX()) <= attackRange
                && Math.abs(actualY - target.getY()) <= attackRange)
            return true;
        return false;
    }
    public boolean isInRange(int x, int y){
        // Performs a check to see if the target piece is within attack range
        if (Math.abs(actualX - x) <= attackRange
                && Math.abs(actualY - y) <= attackRange)
            return true;
        return false;
    }


}
