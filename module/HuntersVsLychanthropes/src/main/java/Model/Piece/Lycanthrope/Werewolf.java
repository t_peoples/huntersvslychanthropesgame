package Model.Piece.Lycanthrope;

import Model.Message.MessageGroup;
import Util.GlobalVariables;

public class Werewolf extends Lycanthrope {

    private final static int hitPoints = 120;
    private final static int attackRange = 1;
    private final static int damage = 25;
    private final static int movementRange = 2;
    private final static String image = GlobalVariables.wereWolfFrame();

    public Werewolf(String id, int initialX, int initialY, MessageGroup response) {
        super(id, hitPoints, attackRange, damage, movementRange, initialX, initialY, false, response,image);
    }

    @Override
    public void setBoostEffect(int daynight) {

        // Boost piece attributes during the night
        if (daynight == GlobalVariables.returnNight() && !isBoosted()) {
            // +0 damage, +0 attack range, +1 movement range
            setBoostEffect(0, 0, 1, true);
        }

        // Revert boost when daytime comes
        else if (daynight == GlobalVariables.returnDay() && isBoosted()) {
            setBoostEffect(0, 0, -1, false);
        }

    }
}
