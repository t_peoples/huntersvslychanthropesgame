package Model.Piece.Lycanthrope;

import Model.Message.MessageGroup;
import Util.GlobalVariables;

public class ShadowWolf extends Lycanthrope{

    private final static int hitPoints = 80;
    private final static int attackRange = 1;
    private final static int damage = 30;
    private final static int movementRange = 3;
    private final static String image = GlobalVariables.shadowWolfFrame();

    public ShadowWolf(String id, int initialX, int initialY, MessageGroup response) {
        super(id, hitPoints, attackRange, damage, movementRange, initialX, initialY, false, response,image);
    }

    @Override
    public void setBoostEffect(int daynight) {

        // Boost piece attributes during the night
        if (daynight == GlobalVariables.returnNight() && !isBoosted()) {
            // +2 damage, +1 attack range, +1 movement range
            setBoostEffect(2, 1, 1, true);
        }

        // Revert boost when daytime comes
        else if (daynight == GlobalVariables.returnDay() && isBoosted()) {
            setBoostEffect(-2, -1, -1, false);
        }

    }

}
