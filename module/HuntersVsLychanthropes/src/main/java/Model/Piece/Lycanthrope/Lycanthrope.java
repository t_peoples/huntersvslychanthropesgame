package Model.Piece.Lycanthrope;

import Model.Message.MessageGroup;
import Model.Piece.Piece;

public class Lycanthrope extends Piece {

    public Lycanthrope(String ID, int hitPoints, int attackRange, int damage, int movementRange, int initialX, int initialY, boolean boosted, MessageGroup response, String image) {
        super(ID, hitPoints, attackRange, damage, movementRange, initialX, initialY, boosted, response, image);
    }

    @Override
    public boolean isBoosted() {
        return false;
    }

    @Override
    public void setBoosted(boolean state) {

    }

    @Override
    public void setBoostEffect(int daynight) {

    }
}
