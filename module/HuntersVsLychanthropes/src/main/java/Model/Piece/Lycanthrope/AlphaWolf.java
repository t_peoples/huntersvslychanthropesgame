package Model.Piece.Lycanthrope;

import Model.Message.MessageGroup;
import Util.GlobalVariables;

public class AlphaWolf extends Lycanthrope{

    private final static int hitPoints = 200;
    private final static int attackRange = 2;
    private final static int damage = 80;
    private final static int movementRange = 2;
    private final static String image = GlobalVariables.alphaWolfFrame();

    public AlphaWolf(String id, int initialX, int initialY, MessageGroup response) {
        super(id, hitPoints, attackRange, damage, movementRange, initialX, initialY, false, response, image);
    }

    @Override
    public void setBoostEffect(int daynight) {

        // Boost piece attributes during the night
        if (daynight == GlobalVariables.returnNight() && !isBoosted()) {
            // +0 damage, +1 attack range, +2 movement range
            setBoostEffect(0, 1, 2, true);
        }

        // Revert boost when daytime comes
        else if (daynight == GlobalVariables.returnDay() && isBoosted()) {
            setBoostEffect(0, -1, -2, false);
        }

    }
}
