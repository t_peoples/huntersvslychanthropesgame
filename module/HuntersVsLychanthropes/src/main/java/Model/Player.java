package Model;

import Model.Piece.PieceInterface;
import Model.Piece.Piece;
import Model.Message.MessageGroup;
import Util.GlobalVariables;
import Util.CircularArrayList;
import Util.TextRepository;

import java.util.ArrayList;
import java.util.List;


public final class Player {

    //player attributes
    private String playerName;
    private Player nextPlayer = null;
    private int undoMoves;
    private ArrayList<PieceInterface> pieces;
    private CircularArrayList<Integer> states;
    private PieceInterface selectedPiece;
    public MessageGroup response;

    public Player(String name,MessageGroup response){
        playerName = name;
        this.pieces = new ArrayList<PieceInterface>();
        this.response = response;
        undoMoves = GlobalVariables.getUndoMoves();
        createStates();
    }
    public Player(String name, ArrayList<PieceInterface> pieces, MessageGroup response){
        playerName = name;
        this.pieces = pieces;
        this.response = response;
        undoMoves = GlobalVariables.getUndoMoves();
        createStates();
    }
    public Player getNextPlayer()
    {
        return this.nextPlayer;
    }
    public void setNextPlayer(Player player)
    {
       nextPlayer = player;
    }
    public void addPiece(Piece piece){
        pieces.add(piece);
    }
    public List<PieceInterface> getPieces() {
        return pieces;
    }
    public String getName(){
        return playerName;
    }
    public PieceInterface getSelectedPiece() {
        return selectedPiece;
    }
    public void changeSelectedPiece(PieceInterface piece){
        pieces.set(pieces.indexOf(selectedPiece), piece);
        selectedPiece = piece;
    }
    public void selectPiece(PieceInterface piece){

        selectedPiece = piece;
        // If selected piece is valid, inform the user that it has been selected
        if (piece != null) {
            response.addMessage(TextRepository.selectedPiece(piece), GlobalVariables.successCode());
        }
        // If invalid piece, inform the user to select another piece
        else {
            response.addMessage(TextRepository.invalidPiece(), GlobalVariables.errorCode());
        }

    }
    // Get Piece location
    public PieceInterface getPieceByCoord(int x, int y) {
        for (PieceInterface temp : pieces) {
            if (x == temp.getX() && y == temp.getY())
                return temp;
        }
        return null;
    }
    public PieceInterface removeDeadPiece(){
        int i = 0;
        for(PieceInterface temp : pieces){
            if(temp.getLife() <=0){
                PieceInterface piece = temp;
                // Remove piece and inform user
                pieces.remove(i);
                response.addMessage(TextRepository.destroyedPiece(piece), GlobalVariables.successCode());
                return piece;
            }
            i++;
        }
        return null;
    }
    public void setPieceBoost(int dayNight){
        for (PieceInterface piece : pieces){
            piece.setBoostEffect(dayNight);
        }
    }
    protected  void createStates(){
        states = new CircularArrayList<Integer>();
        states.add(GlobalVariables.selectState());
        states.add(GlobalVariables.moveState());
        states.add(GlobalVariables.attackState());
    }
    public int getState() {
        return states.getCurrent();
    }
    public boolean setNextState() {
        states.setNext();
        return true;
    }
    public boolean setPrevState(){
        if (isMoving()) {
            states.setPrev();
            return true;
        }
        return false;
    }
    public boolean isAttacking() {
        return states.getCurrent() == GlobalVariables.attackState();
    }
    public boolean isMoving() {
        return states.getCurrent() == GlobalVariables.moveState();
    }
    public boolean isSelecting() {
        return states.getCurrent() == GlobalVariables.selectState();
    }
    public boolean useUndo(){
        if(undoMoves == 0)
            return false;
        undoMoves--;
        return true;
    }
    public MessageGroup getResponse(){
        return response;
    }







}


