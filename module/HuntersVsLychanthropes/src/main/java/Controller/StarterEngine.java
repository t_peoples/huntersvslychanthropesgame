package Controller;

import javafx.application.Application;
import javafx.stage.Stage;

public class StarterEngine extends Application{
    public static void main(String[] args) {
        launch(args);
    }
    public void start(Stage primaryStage) {
        MenuController.build(primaryStage);
    }
}
