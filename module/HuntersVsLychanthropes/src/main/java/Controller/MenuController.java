package Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.Stage;
import Model.Board;
import Util.GlobalVariables;
import Util.Serialization;
import View.RenderFXML;


public class MenuController {
    private static Stage actualStage;
    public static void build(Stage stage){
        actualStage = stage;
        RenderFXML.build(stage, "/view/FXMLMenu.fxml");
    }
    @FXML
    private void initialize() {
    }
    @FXML protected void goToOptions(ActionEvent event){
        OptionsController.build(actualStage);
    }
    @FXML protected void goToLoadGame(ActionEvent event){
        HandleEngine.loadGame(actualStage, (Board) Serialization.deserialize(GlobalVariables.getBoardSavedName()));
    }
    @FXML protected void goToNewGame(ActionEvent event){
        HandleEngine.initNewGame(actualStage);
    }

}