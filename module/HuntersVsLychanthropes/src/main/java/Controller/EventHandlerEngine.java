package Controller;

import java.util.ArrayList;
import java.util.List;

import javafx.stage.Stage;
import Model.Board;
import Model.Player;
import Model.Factory.AbstractFactory;
import Model.Factory.HumanFactory;
import Model.Factory.LycanthropeFactory;
import Model.Message.MessageGroup;
import Model.Piece.Human.Chieftain;
import Model.Piece.Human.Hunter;
import Model.Piece.Human.Ranger;
import Model.Piece.Lycanthrope.AlphaWolf;
import Model.Piece.Lycanthrope.ShadowWolf;
import Model.Piece.Lycanthrope.Werewolf;
import Util.CircularArrayList;
import Util.GlobalVariables;
import Util.Serialization;
import View.DialogBox;
import View.GameRender;



public class EventHandlerEngine {
    private static Board board;
    private static InputEngine engine = null;
    private static List<DialogBox> dialogBoxList;
    private static List<DialogEngine> dialogEngineList = new ArrayList<DialogEngine>();

    public static boolean handleClick(int x, int y) {
        if (engine != null)
            engine.click(x, y);
        return false;
    }

    public static boolean passAction() {
        if (engine != null)
            engine.passAction();
        return false;
    }

    public static boolean backAction() {
        if (engine != null)
            engine.BackAction();
        return false;
    }

    public static boolean defensiveMode() {
        if (engine != null)
            engine.setPieceToDefensiveMode();
        return false;
    }

    public static boolean attackMode() {
        if (engine != null)
            engine.setPieceToAttackMode();
        return false;
    }

    public static boolean normalMode() {
        if (engine != null)
            engine.setSetPieceToNormalMode();
        return false;
    }

    public static boolean UndoAction(Stage actualStage) {
        if (engine != null)
            loadGame(actualStage, engine.Undo());
        return false;
    }

    private static void initialisePlayers(CircularArrayList<Player> players, MessageGroup messageLog) {
        int numPlayers = GlobalVariables.getNumberOfPlayers();

        // Adds players to the list of players
        for (int i = 0; i < numPlayers; i++) {
            Player player = new Player(GlobalVariables.getPlayers(i), messageLog);
            players.add(player);
        }

        initialisePlayerPiece(players, messageLog);
    }

    private static void initialisePlayerPiece(CircularArrayList<Player> players, MessageGroup messageStatus) {
        AbstractFactory factory;

        int numSoldiers = GlobalVariables.numberOfSoldiers();
        int numArchers = GlobalVariables.numberOfArcher();
        int numLeaders = GlobalVariables.numberOfLeaders();

        int initField = GlobalVariables.initField();
        int midFieldX = GlobalVariables.midFieldX();
        int endFieldY = GlobalVariables.endFieldY();

        for (int i = 0; i < GlobalVariables.getNumberOfPlayers(); i++) {

            // First player is always human
            if (i == 0) {
                factory = new HumanFactory();
                for (int j = 0; j < numSoldiers; j++)
                    players.get(i).addPiece(factory.getHuman(new Hunter(), midFieldX - (numSoldiers / 2) + j, initField + 2, messageStatus));
                for (int j = 0; j < numArchers; j++)
                    players.get(i).addPiece(factory.getHuman(new Ranger(), midFieldX - (numArchers / 2) + j, initField + 1, messageStatus));
                for (int j = 0; j < numLeaders; j++)
                    players.get(i).addPiece(factory.getHuman(new Chieftain(), midFieldX - (numLeaders / 2) + j, initField, messageStatus));

                // Second player is always werewolves
            } else {

                factory = new LycanthropeFactory();
                for (int j = 0; j < numSoldiers; j++)
                    players.get(i).addPiece(factory.getLycanthrope(new Werewolf(), midFieldX - (numSoldiers / 2) + j, endFieldY - 2, messageStatus));
                for (int j = 0; j < numArchers; j++)
                    players.get(i).addPiece(factory.getLycanthrope(new ShadowWolf(), midFieldX - (numArchers / 2) + j, endFieldY - 1, messageStatus));
                for (int j = 0; j < numLeaders; j++)
                    players.get(i).addPiece(factory.getLycanthrope(new AlphaWolf(), midFieldX - (numLeaders / 2) + j, endFieldY, messageStatus));

            }
        }
    }

    private static void createBoard(MessageGroup messageLog, MessageGroup messageStatus) {
        CircularArrayList<Player> players = new CircularArrayList<Player>();
        initialisePlayers(players, messageLog);
        board = new Board(players, messageStatus, 4);
    }

    public static void initNewGame(Stage stage) {
        MessageGroup messageLog = new MessageGroup();
        dialogBoxList = new ArrayList<DialogBox>();
        DialogBox dialogLog = new DialogBox("Log", true);
        DialogEngine logDialogEngine = new DialogEngine(messageLog, dialogLog);

        // The messageStatus is showed on the dialogStatus and
        // controlled by statusDialogEngine, messageLog is used by Board
        MessageGroup messageStatus = new MessageGroup();
        DialogBox dialogStatus = new DialogBox("Status", false);
        DialogEngine statusDialogEngine = new DialogEngine(messageStatus, dialogStatus);


        dialogBoxList.add(dialogStatus);
        dialogBoxList.add(dialogLog);


        // At this point all instances had been created and
        // InputEngine have access to the model with board and the view with render


        dialogEngineList.add(statusDialogEngine);
        dialogEngineList.add(logDialogEngine);

        createBoard(messageLog, messageStatus);

        GameRender gameRender = new GameRender(stage, board, dialogBoxList);
        engine = new InputEngine(board, dialogEngineList, gameRender);

    }

    public static void goToMenuView(Stage actualStage) {
        MenuController.build(actualStage);
    }

    public static void saveState() {
        Serialization.serialize(board, GlobalVariables.getBoardSavedName());
    }

    public static void loadGame(Stage actualStage, Board loadBoard) {
        if (loadBoard == null)
            return;
        board = loadBoard;
        MessageGroup messageLog = board.getPlayers().get(0).getResponse();
        dialogBoxList = new ArrayList<DialogBox>();
        DialogBox dialogLog = new DialogBox("Log", true);
        DialogEngine logDialogEngine = new DialogEngine(messageLog, dialogLog);

        // The messageStatus is showed on the dialogStatus and
        // controlled by statusDialogEngine, messageLog is used by Board
        MessageGroup messageStatus = board.getResponse();
        DialogBox dialogStatus = new DialogBox("Status", false);
        DialogEngine statusDialogEngine = new DialogEngine(messageStatus, dialogStatus);


        dialogBoxList.add(dialogStatus);
        dialogBoxList.add(dialogLog);


        // At this point all instances had been created and
        // InputEngine have access to the model with board and the view with render


        dialogEngineList.add(statusDialogEngine);
        dialogEngineList.add(logDialogEngine);

        GameRender gameRender = new GameRender(actualStage, loadBoard, dialogBoxList);
        engine = new InputEngine(board, dialogEngineList, gameRender);

    }
}
