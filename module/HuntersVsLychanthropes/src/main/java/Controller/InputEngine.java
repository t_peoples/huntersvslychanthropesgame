package Controller;

import java.util.List;

import Model.Board;
import Model.Command.ActionList;
import Model.Command.ForwardAction;
import Model.Command.BackAction;
import Util.GlobalVariables;
import View.GameRender;

public class InputEngine implements java.io.Serializable {

    private Board board; // Controller
    private List<DialogEngine> dialogEngineList;
    private GameRender gameRender;
    private ActionList actionList = new ActionList(new BackAction(), new ForwardAction());


    public InputEngine(Board board, List<DialogEngine> dialogEngineList, GameRender gameRender) {
        this.board = board;
        this.dialogEngineList = dialogEngineList;
        this.gameRender = gameRender;
        actionList.nextState(board);
        Refresh();
    }

    public void click(int x, int y) {
        selectState(x, y);
        movState(x, y);
        attackState(x, y);
        Refresh();
    }

    private boolean isSelectState() {
        return board.getState() == GlobalVariables.selectState();
    }

    private boolean isMoveState() {
        return board.getState() == GlobalVariables.moveState();
    }

    private boolean isAttackState() {
        return board.getState() == GlobalVariables.attackState();
    }

    public void passAction() {
        if (isMoveState()) {
            board.nextState();
            gameRender.printBoard(board);
            gameRender.printAttackRange(board.getCurrentPlayerTurn().getSelectedPiece());
        } else if (isAttackState()) {
            board.nextState();
            gameRender.printBoard(board);
            actionList.nextState(board);
        }
        Refresh();
    }

    public void BackAction() {
        board.prevState();
        gameRender.printBoard(board);
        Refresh();
    }

    public void setDefensiveMode() {
        board.SetBlockDefensive();
        gameRender.printBoard(board);
        skipPlayerTurn();
    }

    public void setAttackMode() {
        board.SetBlockAttack();
        gameRender.printBoard(board);
        skipPlayerTurn();
    }

    public void setNormalMode() {
        board.SetBlockNormal();
        gameRender.printBoard(board);
        skipPlayerTurn();

    }

    private void skipPlayerTurn() {
        board.nextState();
        Refresh();
        board.nextState();
        Refresh();
        actionList.nextState(board);
    }
    private void attackState(int x, int y){
        if (!isAttackState()) {
            return;
        }
        if (board.attackPiece(x, y)) {
            gameRender.printBoard(board);
            actionList.nextState(board);
        }

    }
    private void movState(int x, int y){
        if (!isMoveState()) {
            return;
        }
        if(board.movePiece(x, y)){
            // Show the attack range of the selected piece
            gameRender.printBoard(board);
            gameRender.printAttackRange(board.getCurrentPlayerTurn().getSelectedPiece());
        }


    }
    private void selectState(int x, int y){
        if (!isSelectState()) {
            return;
        }
        if (board.selectPiece(x, y)) {
            // Show the movement range of the selected piece
            gameRender.printMoveRange(board.getCurrentPlayerTurn().getSelectedPiece());
        }
    }


    public Board Undo() {
        Board aux = null;
        if(board.getCurrentPlayerTurn().useUndo()){
            aux = actionList.lastState(board);
            if(aux != null)
                board = aux;
        }
        return aux;

    }
    private void Refresh() {
        for (DialogEngine temp : dialogEngineList) {
            temp.getDialog().setMessage(temp.getMessageGroup());
            temp.getMessageGroup().clearMessageList();
        }

        gameRender.setButtonView(board);
    }

}
