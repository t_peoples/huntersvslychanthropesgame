package Controller;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import Util.GlobalVariables;
import Util.InitVariables;
import Util.Serialization;
import View.RenderFXML;


public class OptionsController {

    @FXML private GridPane gridPane;

    @FXML private CheckBox checkPlayer1;
    @FXML private CheckBox checkPlayer2;
    @FXML private CheckBox checkPlayer3;
    @FXML private CheckBox checkPlayer4;

    @FXML private TextField namePlayer1;
    @FXML private TextField namePlayer2;
    @FXML private TextField namePlayer3;
    @FXML private TextField namePlayer4;

    @FXML private TextField soldierPieces;
    @FXML private TextField archerPieces;
    @FXML private TextField leaderPieces;
    @FXML private TextField tOfDayDuration;
    @FXML private TextField hBlocks;
    @FXML private TextField wBlocks;
    @FXML private TextField sBlocks;
    @FXML private CheckBox circularCheckBox;

    public static InitVariables initVar = (InitVariables) Serialization.deserialize(GlobalVariables.getOptionsFile());
    private static Stage actualStage;

    public static void build(Stage stage){
        actualStage = stage;
        RenderFXML.build(stage, "/view/FXMLOptions.fxml");
    }
    @FXML
    private void initialize() {
        setFields();
    }
    @FXML protected void handleBack(ActionEvent event){
        MenuController.build(actualStage);
    }
    @FXML protected void handleCheck(ActionEvent event){
        CheckBox aux = ((CheckBox)event.getSource());
        for(int i=1;i<5;i++)
            if(aux==((CheckBox)gridPane.lookup(GlobalVariables.checkBoxPlayer()+i)))
                if(((TextField)gridPane.lookup(GlobalVariables.namePlayer()+i)).isVisible())
                    ((TextField)gridPane.lookup(GlobalVariables.namePlayer()+i)).setVisible(false);
                else
                    ((TextField)gridPane.lookup(GlobalVariables.namePlayer()+i)).setVisible(true);
    }
    @FXML protected void handleSave(ActionEvent event){
        initVar = new InitVariables(paleyersToString(), gameOptionsToString(), circularCheckBox.isSelected());
        Serialization.serialize(initVar, GlobalVariables.getOptionsFile());
        refreshInitVar();
    }
    @FXML protected void handleResetFields(ActionEvent event){
        resetFields();
    }
    private void setFields(){
        initVar = (InitVariables) Serialization.deserialize(GlobalVariables.getOptionsFile());
        if(initVar == null){
            return;
        }
        for(int i=1;i<5;i++)
            if(initVar.getPlayer(i-1)!=null){
                ((CheckBox)gridPane.lookup(GlobalVariables.checkBoxPlayer()+i)).setSelected(true);
                ((TextField)gridPane.lookup(GlobalVariables.namePlayer()+i)).setText(initVar.getPlayer(i-1));
                ((TextField)gridPane.lookup(GlobalVariables.namePlayer()+i)).setVisible(true);
            }
        soldierPieces.setText(Integer.toString(initVar.getNumberOfSoldierPieces()));
        archerPieces.setText(Integer.toString(initVar.getNumberOfArchers()));
        leaderPieces.setText(Integer.toString(initVar.getNumberOfLeaders()));
        wBlocks.setText(Integer.toString(initVar.getWidthBlocks()));
        hBlocks.setText(Integer.toString(initVar.getHeightBlocks()));
        sBlocks.setText(Integer.toString(initVar.getSizeOfBlocks()));
        tOfDayDuration.setText(Integer.toString(initVar.getTimeOfDay()));
        circularCheckBox.setSelected(initVar.isCircularBoard());
    }
    private void resetFields(){
        for(int i=1;i<5;i++){
            if(((CheckBox)gridPane.lookup(GlobalVariables.checkBoxPlayer()+i)).isSelected())
                ((CheckBox)gridPane.lookup(GlobalVariables.checkBoxPlayer()+i)).setSelected(false);
            ((TextField)gridPane.lookup(GlobalVariables.namePlayer()+i)).setText("");
            ((TextField)gridPane.lookup(GlobalVariables.namePlayer()+i)).setVisible(false);
        }
        soldierPieces.setText(GlobalVariables.defaultSoldierPieces());
        archerPieces.setText(GlobalVariables.defaultArcherPieces());
        leaderPieces.setText(GlobalVariables.defaultLeaderPieces());
        tOfDayDuration.setText(GlobalVariables.defaultTimeOfDayDuration());
        wBlocks.setText(GlobalVariables.defaultWidthBlocks());
        hBlocks.setText(GlobalVariables.defaultHeightBlocks());
        sBlocks.setText(GlobalVariables.defaultSizeOfBlocks());

    }
    private ArrayList<String> playersToString(){
        ArrayList<String> aux = new ArrayList<String>();
        for(int i=1;i<5;i++)
            if(((CheckBox)gridPane.lookup(GlobalVariables.checkBoxPlayer()+i)).isSelected())
                aux.add(((TextField)gridPane.lookup(GlobalVariables.namePlayer()+i)).getText());
        return aux;
    }






}
