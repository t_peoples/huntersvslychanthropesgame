package Controller;

import Model.Message.MessageGroup;
import View.DialogBox;

public class DialogEngine implements java.io.Serializable{

    private MessageGroup messageGroup;
    private DialogBox dialog;

    public DialogEngine(MessageGroup messageGroup,DialogBox dialog){
        this.dialog = dialog;
        this.messageGroup = messageGroup;
    }
    public MessageGroup getMessageGroup(){
        return messageGroup;
    }
    public DialogBox getDialog(){
        return dialog;
    }


}
