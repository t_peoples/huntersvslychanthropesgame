package View;

import Model.Message.Message;
import Model.Message.MessageGroup;
import Util.GlobalVariables;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.control.TextArea;

import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

public class DialogBox extends GridPane implements java.io.Serializable {

    private TextArea textPane = new TextArea();
    // If this is set as true, the message will be cleaned each user action,
    // no matter if there is or not another message to replace it
    private boolean cleanAlways;

    private DialogBox(String title,boolean cleanAlways){
        addRow(0,textPane);
        setVisible(true);
        this.cleanAlways = cleanAlways;
    }
    private void appendToPane(TextArea textPane,String message,Color colour){
        StyleContext styleContext = StyleContext.getDefaultStyleContext();
        AttributeSet attributeSet = styleContext.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, colour);

        attributeSet = styleContext.addAttribute(attributeSet,StyleConstants.FontFamily,"Lucida Console");
        attributeSet = styleContext.addAttribute(attributeSet,StyleConstants.Alignment,StyleConstants.ALIGN_JUSTIFIED);
        textPane.replaceSelection(message);
    }

    public void setMessage(MessageGroup messageControl){
        String newline = System.getProperty("line.separator");
        // If cleanAlways is false, dont matter if it dont has a valid message, it will not clear the tPane, dont do nothing
        if (!messageControl.hasValidMessage() && cleanAlways == false)
            return;
        textPane.setText(null);
        //TODO Refactor
        // Set color of message according to the type
        for (Message temp : messageControl.getMessageList()) {
            if (temp.getMessageType() == GlobalVariables.successCode()) {
                appendToPane(textPane, temp.getMessageText(), Color.GREEN);
                appendToPane(textPane, newline + "----------------" + newline, Color.GREEN);
            } else if (temp.getMessageType() == GlobalVariables.errorCode()) {
                appendToPane(textPane, temp.getMessageText(), Color.RED);
                appendToPane(textPane, newline + "----------------" + newline, Color.RED);
            } else if (temp.getMessageType() == GlobalVariables.informationCode()) {
                appendToPane(textPane, temp.getMessageText(), Color.BLUE);
                appendToPane(textPane, newline + "----------------" + newline, Color.BLUE);
            } else if (temp.getMessageType() == GlobalVariables.eventCode()) {
                appendToPane(textPane, temp.getMessageText(), Color.GRAY);
                appendToPane(textPane, newline + "----------------" + newline, Color.GRAY);
            }
        }

    }

}
