package View;

import Controller.HandleEngine;
import Model.Board;
import Util.GlobalVariables;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ButtonBar extends GridPane {

    private Button defensiveModeButton = new Button("Defensive Mode");
    private Button attackModeButtton = new Button("Attack Mode");
    private Button normalModeButton = new Button("Normal Mode");

    private Button passButton = new Button("Pass");
    private Button backButton = new Button("Back");
    private Button undoButton = new Button("Undo");

    public ButtonBar(Stage stage) {

        defensiveModeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                HandleEngine.DefMod();
            }
        });
        attackModeButtton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                HandleEngine.AtkMod();
            }
        });
        passButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                HandleEngine.PassAction();
            }
        });
        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                HandleEngine.BackAction();
            }
        });
        normalModeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                HandleEngine.NrlMod();
            }
        });
        undoButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                HandleEngine.UndoAction(stage);
            }
        });

        add(defensiveModeButton, 0, 0);
        add(attackModeButtton, 1, 0);
        add(normalModeButton, 2, 0);
        add(passButton, 3, 0);
        add(backButton, 4, 0);
        add(undoButton, 5, 0);
    }
    public void setView(Board board){
        if(board.getState() == GlobalVariables.selectState()){
            setSelectView();
        }else if(board.getState() == GlobalVariables.moveState()){
            setMoveView();
        }else if(board.getState() == GlobalVariables.attackState()){
            setAttackView();
        }
    }
    private void setSelectView(){
        defensiveModeButton.setVisible(false);
        attackModeButtton.setVisible(false);
        normalModeButton.setVisible(false);
        passButton.setVisible(false);
        backButton.setVisible(false);
        undoButton.setVisible(true);
    }
    private void setMoveView(){
        defensiveModeButton.setVisible(true);
        attackModeButtton.setVisible(true);
        normalModeButton.setVisible(true);
        passButton.setVisible(true);
        backButton.setVisible(true);
        undoButton.setVisible(false);
    }
    private void setAttackView(){
        defensiveModeButton.setVisible(false);
        attackModeButtton.setVisible(false);
        normalModeButton.setVisible(false);
        passButton.setVisible(true);
        backButton.setVisible(false);
        undoButton.setVisible(false);
    }

}
