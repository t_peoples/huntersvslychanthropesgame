package View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class InputBox extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private JTextField textFieldWerewolf;
    private JTextField textFieldHunter;
    private DoubleJTextField numPieces;

    public InputBox() {
        setBounds(100, 100, 309, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setLayout(new FlowLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        {
            JLabel labelNewLabel = new JLabel("Player WereWolf name:");
            contentPanel.add(labelNewLabel);
        }
        {
            textFieldWerewolf = new JTextField();
            contentPanel.add(textFieldWerewolf);
            textFieldWerewolf.setColumns(10);
        }
        {
            JLabel labelNewLabel = new JLabel("Player hunter name:");
            contentPanel.add(labelNewLabel);
        }
        {
            textFieldHunter = new JTextField();
            contentPanel.add(textFieldHunter);
            textFieldHunter.setColumns(10);
        }
        {
            JLabel labelNewLabel = new JLabel("Number of pieces");
            contentPanel.add(labelNewLabel);
        }
        {
            numPieces = new DoubleJTextField();
            contentPanel.add(numPieces);
            numPieces.setColumns(10);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                setVisible(true);
                okButton.addActionListener(
                        new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent event) {
                                /* What do we want to happen when we
                                click the button */
                                JOptionPane.showInputDialog("Gla");
                            }

                        });
            }
        }
    }
}
//TODO Refactor into seperate class perhaps
public class DoubleJTextField extends JTextField {
    public DoubleJTextField() {
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char ch = e.getKeyChar();
                if (!isNumber(ch) && !isValidSignal(ch) && !validatePoint(ch) && ch != '\b')
                {
                    e.consume();
                }
            }
        });
    }
    private boolean isNumber(char ch){
        return ch >= '0' && ch <= '9';
    }
    private boolean isValidSignal(char ch) {
        if ((getText() == null || "".equals(getText().trim())) && ch == '-')
        {
            return true;
        }
        return false;
    }
    private boolean validatePoint(char ch){
        if (ch != '.') {
            return false;
        }
        if (getText() == null || "".equals(getText().trim())) {
            setText("0.");
            return false;
        } else if ("-".equals(getText())) {
            setText("-0.");
        }
        return true;
    }

}
