package View;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class RenderFXML {
    public static final void build(Stage stage, String screen){
        try{
            GridPane page = (GridPane) FXMLLoader.load(RenderFXML.class.getResource(screen));
            Scene scene = new Scene(page);
            stage.setScene(scene);
            stage.show();
        }catch (Exception ex) {
            System.err.println( ex.getClass().getName() + ": " + ex.getMessage() );
        }

    }
}
