package View;
import java.util.HashMap;
import java.util.List;

import Model.Board;
import Model.Piece.PieceInterface;

import Controller.EventHandlerEngine;

import Util.GlobalVariables;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import javafx.scene.Scene;

public class GameRender {
    private final String token = "#";
    private GridPane page;
    private HashMap<String, Button> buttonList = new HashMap<String, Button>();
    private ButtonBar buttonBar;
    private MenuBar menu;
    private Stage actualStage;

    public GameRender(Stage stage, Board board, List<DialogBox> list){
        try {
            buttonBar = ButtonBar.getInstance(stage);
            actualStage = stage;
            page = new GridPane();

            createMenu();
            createButtons(board);
            createDialogBoxes(board, list);
            page.add(buttonBar, 0, GlobalVariables.Height() + 2, GlobalVariables.numberOfBlocksWidth(), 1);

            Scene scene = new Scene(page);
            stage.setScene(scene);
            stage.show();

        }catch (Exception ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
    }
    private static boolean isNight(Board board){
        if(board.getTimeOfDay() == GlobalVariables.returnNight())
            return true;
        return false;
    }

    public final void printBoard(Board board){
        ColorAdjust colourAdjust = new ColorAdjust();
        if (isNight(board))
            colourAdjust.setBrightness(-0.5);
        for (int i = 0; i < GlobalVariables.numberOfBlocksHeight(); i++) {
            for (int j = 0; j < GlobalVariables.numberOfBlocksWidth(); j++) {
                getButton(j, i).setGraphic(board.getBlock(j, i).getImage());
                getButton(j, i).setEffect(colourAdjust);
            }
        }
    }
    public final void printMoveRange(PieceInterface piece){
        ColorAdjust colourAdjust = new ColorAdjust();
        colourAdjust.setHue(0.1);
        for (int i = 0; i < GlobalVariables.numberOfBlocksHeight(); i++) {
            for (int j = 0; j < GlobalVariables.numberOfBlocksWidth(); j++) {
                if (piece.checkMove(j, i))
                    getButton(j, i).setEffect(colourAdjust);
            }
        }
    }

    public void handleClick(Button button) {
        String[] string = button.getId().split(token);
        EventHandlerEngine.handleClick(Integer.parseInt(string[0]), Integer.parseInt(string[1]));

    }
    private Button getButton(int i, int j) {
        return buttonList.get(i + token + j);
    }
    public void setButtonView(Board board) {
        buttonBar.setView(board);
    }
    public void createDialogBoxes(Board board, List<DialogBox> list) {
        int count = 0;
        int collSpan = GlobalVariables.numberOfBlocksHeight() / list.size();
        for (DialogBox aux : list) {
            aux.setMaxHeight(GlobalVariables.Width() / list.size());
            page.add(aux, GlobalVariables.numberOfBlocksWidth() + 1, (collSpan * count) + 1, 1, collSpan);
            count++;
        }
    }
    public void createButtons(Board board) {
        ColorAdjust colorAdjust = new ColorAdjust();
        // If the time is night, darken the board to notify the player.
        if (isNight(board)) {
            colorAdjust.setBrightness(-0.5);
        }
        // Creates the board, allows the use of buttons for each player Piece
        for (int i = 0; i < GlobalVariables.numberOfBlocksHeight(); i++) {
            for (int j = 0; j < GlobalVariables.numberOfBlocksWidth(); j++) {
                Button aux = new Button();
                aux.setGraphic(board.getBlock(j, i).getImage());
                aux.setEffect(colorAdjust);
                aux.setPrefSize(GlobalVariables.sizeOfBlock(), GlobalVariables.sizeOfBlock());
                aux.setMinSize(GlobalVariables.sizeOfBlock(), GlobalVariables.sizeOfBlock());
                aux.setMaxSize(GlobalVariables.sizeOfBlock(), GlobalVariables.sizeOfBlock());
                aux.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        handleClick((Button) e.getSource());
                    }
                });
                aux.setId(j + token + i);
                buttonList.put(j + token + i, aux);
                page.add(aux, j, i + 1);
            }
        }
    }

    public void createMenu() {
        menu = new MenuBar();
        Menu menuGame = new Menu("Game");
        MenuItem itemQuit = new MenuItem("Quit");
        itemQuit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                EventHandlerEngine.goToMenuView(actualStage);
            }
        });
        MenuItem itemSave = new MenuItem("Save");
        itemSave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                EventHandlerEngine.saveState();
            }
        });
        menuGame.getItems().add(itemSave);
        menuGame.getItems().add(itemQuit);
        menu.getMenus().add(menuGame);
        page.add(menu, 0, 0, GlobalVariables.numberOfBlocksWidth(), 1);
    }

    public final void printAttackRange(PieceInterface piece) {
        ColorAdjust colorAdjust = new ColorAdjust();
        colorAdjust.setHue(-0.1);
        for (int i = 0; i < GlobalVariables.numberOfBlocksHeight(); i++) {
            for (int j = 0; j < GlobalVariables.numberOfBlocksWidth(); j++) {
                if (piece.isInRange(j, i))
                    getButton(j, i).setEffect(colorAdjust);
            }
        }
    }

}
