package View;

import java.awt.Color;
import java.awt.image.BufferedImage;
import Model.Board;
import Model.Piece.Piece;

public interface Render {

    // Print the entire board according to board instance
    public void drawBoard(Board board);

    // Return a instance of the screen
    public BufferedImage getScreen();

    // Print the entire board with a range
    public void printBoardRange(Piece a, Color colour, int range, Board board);
}
