package Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serialization {

    public static void serialize(Object object,String file){
        try{

            FileOutputStream fileOutput = new FileOutputStream(file);
            ObjectOutputStream outputStream = new ObjectOutputStream(fileOutput);
            outputStream.writeObject(object);
            outputStream.close();
            fileOutput.close();

        }catch(IOException i){
            i.printStackTrace();
        }
    }

    public static Object deserialize(String file) {
        Object object =null;

        try{
            FileInputStream fileInput = new FileInputStream(file);
            ObjectInputStream inputStream = new ObjectInputStream(fileInput);
            object = inputStream.readObject();
            inputStream.close();
            fileInput.close();
        }catch(IOException i){
            i.printStackTrace();
            return null;
        }catch(ClassNotFoundException c)
        {
            c.printStackTrace();
            return null;
        }
        return object;
    }
    public static void delete(String data){
        try
        {
            File file = new File(data);
            file.delete();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
