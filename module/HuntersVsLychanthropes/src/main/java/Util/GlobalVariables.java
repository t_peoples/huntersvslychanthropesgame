package Util;

import java.awt.Color;
import java.util.Random;

import Controller.OptionsController;

public class GlobalVariables {

    private static InitVariables initVar = OptionsController.initVar;
    public final static void setInitVar(){
        initVar = OptionsController.initVar;
    }
    public final static boolean isCircularBoard(){
        return initVar.isCircularBoard();
    }
    public final static int Height() {
        return numberOfBlocksHeight() * sizeOfBlock();
    }
    public final static int Width() {
        return numberOfBlocksWidth() * sizeOfBlock();
    }
    public final static int numberOfBlocksHeight() {
        return initVar.getHeightBlocks();
    }
    public final static int numberOfBlocksWidth() {
        return initVar.getWidthBlocks();
    }
    public final static int sizeOfBlock() {
        return numberOfBlocksHeight() * numberOfBlocksWidth();
    }
    public final static char successCode() {
        return 'S';
    }
    public final static char errorCode() {
        return 'E';
    }
    public final static char eventCode() {
        return 'C';
    }
    public static int getUndoMoves() {
        return 3;
    }
    public final static int selectState() {
        return 1;
    }
    public final static int moveState() {
        return 2;
    }
    public final static int attackState() {
        return 3;
    }
    public final static String grassFrame(){
        return "res/grass.png";
    }
    public static String rockFrame() {
        return "res/rock.png";
    }
    public final static String alphaWolfFrame(){
        return "res/wolf3.png";
    }
    public final static String shadowWolfFrame(){
        return "res/wolf2.png";
    }
    public final static String wereWolfFrame(){
        return "res/wolf1.png";
    }
    public final static String chieftainFrame(){
        return "res/man3.png";
    }
    public final static String hunterFrame(){
        return "res/man2.png";
    }
    public final static String rangerFrame(){
        return "res/man1.png";
    }
    public final static char informationCode() {
        return 'I';
    }
    public final static int returnDay() {
        return 1;
    }
    public final static int returnNight() {
        return 2;
    }
    public static String getTrackFolder() {
        return "track/";
    }
    public static String getBoardSavedName() {

        return "saves/board.ser";
    }
    public final static int numberOfSoldiers(){
        return initVar.getNumberOfSoldierPieces();
    }
    public final static int numberOfArcher(){
        return initVar.getNumberOfArchers();
    }
    public final static int numberOfLeaders(){
        return initVar.getNumberOfLeaders();
    }
    public final static int initField() {
        return 0;
    }
    public final static int midFieldX() {
        return Width() / (2 * sizeOfBlock());
    }
    public final static int endFieldY() {
        return (Height() - sizeOfBlock()) / sizeOfBlock();
    }
    public final static int getNumberOfPlayers(){
        return initVar.getNumberOfPlayers();
    }
    public final static String checkBoxPlayer(){
        return "#checkPlayer";
    }
    public final static String namePlayer(){
        return "#namePlayer";
    }
    public static String getOptionsFile(){
        return "saves/options.ser";
    }
    public static String defaultSoldierPieces(){
        return Integer.toString(5);
    }
    public static String defaultArcherPieces(){
        return Integer.toString(3);
    }
    public static String defaultLeaderPieces(){
        return Integer.toString(1);
    }
    public static String defaultTimeOfDayDuration(){
        return Integer.toString(4);
    }
    public static String defaultHeightBlocks(){
        return Integer.toString(20);
    }
    public static String defaultWidthBlocks(){
        return Integer.toString(20);
    }
    public static String defaultSizeOfBlocks(){
        return Integer.toString(60);
    }
    public final static String getPlayers(int index){
        return initVar.getPlayer(index);
    }
    public static String hunterId() {
        return "Hunter";
    }
    public static String chieftainId() {
        return "Chieftain";
    }
    public static String rangerId() {
        return "Ranger";
    }
    public static String alphaWolfId() {
        return "Alpha Wolf";
    }
    public static String werewolfId() {
        return "Werewolf";
    }
    public static String shadowWolfId() {
        return "Shadow Wolf";
    }
    public static String defaultPlayer1() {
        return "Player 1";
    }
    public static String defaultPlayer2() {
        return "Player 2";
    }
    public static String getOptionView() {
        return "/view/FXMLOptions.fxml";
    }
    public static String getMenuView() {
        return "/view/FXMLMenu.fxml";
    }



}
