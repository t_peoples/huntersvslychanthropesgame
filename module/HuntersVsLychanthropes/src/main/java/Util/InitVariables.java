package Util;

import java.util.ArrayList;

public class InitVariables {

    private final String totem = "#";
    private String Players = "";
    private String GameOptions = "";
    private boolean CircularBoard = false;

    public InitVariables(ArrayList<String> players, ArrayList<String> gameOptions, boolean circular)
    {
        for(String aux: players)
            if(aux!=null)
                Players = Players+aux+totem;
        for(String aux: gameOptions)
            if(aux!=null)
                GameOptions = GameOptions+aux+totem;
        CircularBoard = circular;
    }
    public int getNumberOfPlayers(){
        String [] array = Players.split(totem);
        return array.length;
    }
    public String getPlayer(int index){
        if(index >= getNumberOfPlayers())
            return null;
        String [] array = Players.split(totem);
        return array[index];
    }
    public int getNumberOfArchers(){
        return getGameOptionsByIndex(1);
    }
    public int getNumberOfSoldierPieces(){
        return getGameOptionsByIndex(0);
    }
    public int getNumberOfLeaders(){
        return getGameOptionsByIndex(2);
    }
    public int getHeightBlocks(){
        return getGameOptionsByIndex(3);
    }
    public int getWidthBlocks(){
        return getGameOptionsByIndex(4);
    }
    public int getSizeOfBlocks(){
        return getGameOptionsByIndex(5);
    }
    public int getTimeOfDay(){
        return getGameOptionsByIndex(6);
    }
    public boolean isCircularBoard(){
        return CircularBoard;
    }
    public int getGameOptionsByIndex(int index){
        String [] array = GameOptions.split(totem);
        int i = (Integer.parseInt(array[index]));
        return i;
    }



}
