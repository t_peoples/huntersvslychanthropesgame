package Util;

import Model.Piece.PieceInterface;
import Model.Player;

public class TextRepository {

    static private String newLine = System.getProperty("line.separator");
    static private String pressEnterToPass = "You may also press 'ENTER' to pass.";

    public static String selectedPiece(PieceInterface piece){
        return "You have selected:" + piece.getID() + newLine + "HP:" + piece.getLife()  + newLine
                + "Damage: "
                + piece.getDamage();
    }

    public static String destroyedPiece(PieceInterface piece){
        return "The piece:" + piece.getID() + " has been destroyed";
    }

    public static String  outOfMovementRange(){
        return "Out of movement range!";
    }
    public static String attackSuccess(int attack, PieceInterface target){
        return target.getID() + " has dealt " + attack + " damage!";
    }
    public static String outOfAttackRange(){
        return "Out of attack range!";
    }
    public static  String playerTurn(Player player){
        String aux = "It is your turn: "
                + player.getName()
                + newLine
                + newLine
                + "Please select one of your pieces:"
                + newLine
                + newLine
                + "&P1";
        String pieceSummary = "";

        for (PieceInterface temp : player.getPieces()){
            pieceSummary += "-"
                    + temp.getID()
                    + "  HP: "
                    + Integer.toString(temp.getLife())
                    + newLine;
        }
        aux = aux.replace("&P1",pieceSummary);
        return aux;
    }

    public static String invalidPiece(){
        return "The selected piece was invalid.";
    }
    public static String selectTarget(){
        return "Please select a piece within the red tiles to attack."
                + newLine
                + newLine
                + pressEnterToPass;
    }
    public static String dayEffect() {
        return "The sun is shining bright."
                + newLine
                + newLine
                + "The humans bask in its light and feel stronger.";
    }

    public static String nightEffect() {
        return "The moon emits a deathly glow."
                + newLine
                + newLine
                + "The werewolves howl and grow stronger.";
    }
    public static String selectMove() {
        return "Please select a block within the blue tiles."
                + newLine
                + newLine
                + pressEnterToPass
                + newLine
                + "You may also press 'BACKSPACE' to select another piece.";
    }


}
