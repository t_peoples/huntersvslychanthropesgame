package Util;

import java.util.ArrayList;

public class CircularArrayList<E> extends ArrayList<E>
{
    private int actual;

    public boolean add(E e){
        actual = 0;
        return super.add(e);
    }

    public E get(int index){
        if (index == -1)
        {
            index = size() - 1;
        } else if (index == size())
        {
            index = 0;
        } else if (index >= size())
        {
            return get(index - size());
        }
        return super.get(index);
    }

    public void setPrev(){
        actual--;
    }
    public void setNext(){
        actual++;
    }
    public E getCurrent(){
        return get(actual);
    }
    public boolean isOnFirst(){
        if(actual%size() == 0)
            return true;
        return  false;
    }

}
