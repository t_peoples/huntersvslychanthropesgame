package Util;

public class UtilFunctions {
    public static boolean isNumber(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
}
